﻿using Recept_Builder_1_1.GUI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Recept_Builder_1_1
{
    class ReceptBuilderForm : Form
    {
        public ReceptBuilderForm()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.Text = "Recept Builder 1.1";            
            this.AutoScroll = true;
            this.AutoSize = true;
            ReceptPanel panel = new ReceptPanel();
            this.Controls.Add(panel);
            this.CenterToScreen();
            this.Visible = true;
        }
    }
}
