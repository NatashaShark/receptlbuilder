﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recept_Builder_1_1.SCRecept
{
    public class LeadTime
    {
        private double amount;
        private string dimention;
        private LeadTime()
        {

        }
        private LeadTime(double amount, String dimention)
        {
            this.Amount = amount;
            this.Dimention = dimention;
        }

        public double Amount
        {
            get
            {
                return this.amount;
            }
            set
            {
                if (value > 0)
                {
                    this.amount = value;
                }
                else throw new ArgumentOutOfRangeException();
            }
        }

        public string Dimention 
        {
            get 
            {
                return this.dimention;
            }

            set
            {
                if (value != null && value != "")
                {
                    this.dimention = value;
                }
                else throw new ArgumentException();
            }
        }
    }
}
