﻿using Recept_Builder_1_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recept_Builder_1_1.SCRecept;

namespace Recept_Builder_1._1
{
    public class Recept
    {
        private String name;
        private List<Ingredient> ingredientList;
        private List<InstructionStep> instructionList;
        private Result finishedDish;

        public Recept()
        {
            this.ingredientList = new List<Ingredient>();
        }

        public Recept (String name) : this()
        {
            if (name == null && name == "")
            {
                throw new ArgumentException();
            }
            this.name = name;
        }        

        public Recept(String name, List<Ingredient> ingredientList, List<InstructionStep> instructionList, Result finishedDish)
            : this(name)
        {
            this.ingredientList = ingredientList;
            this.instructionList = instructionList;
            this.finishedDish = finishedDish;
        }

        public void Add (Ingredient ingredient)
        {
            ingredientList.Add(ingredient);
        }
        public void remove (Ingredient ingredient)
        {
            ingredientList.Remove (ingredient);
        }

        public void Add (InstructionStep instruction)
        {
            instructionList.Add(instruction);
        }
        public void Remove (InstructionStep instruction)
        {
            instructionList.Remove(instruction);
        }

        public List<Ingredient> Ingredients {
            get {
                return this.ingredientList;
            }
            set
            {
                this.ingredientList = value;
            }
            
        }

        public List<InstructionStep> Instruction
        {
            get {
                return this.instructionList;
            }

            set
            {
                this.instructionList = value;
            }
            
        }

        public String Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        public Result Result
        {
            get
            {
                return this.finishedDish;
            }
            set
            {
                this.finishedDish = value;
            }
        }

        public override string ToString()
        {
            String recept = "";
            recept += this.name;
            recept += "\nINGREDIENTS:";
            foreach (Ingredient ingredient in ingredientList)
            {
                recept += "\n" + ingredient.ToString();
            }
            recept += "\nINSTRUCTION:";
            foreach (InstructionStep instruction in instructionList)
            {
                recept += "\n" + instruction.ToString();
            }
            recept += "\nRESULT:";
            recept += finishedDish.ToString();
            return recept;
        }

    }
}
