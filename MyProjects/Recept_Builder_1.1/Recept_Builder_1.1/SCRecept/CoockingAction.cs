﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1.SCRecept
{
    public class CoockingAction
    {
        private String name;

        public CoockingAction()
        {
        }

        public CoockingAction(String name) 
        {
            this.name = name;
        }

        public String Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
    }
}
