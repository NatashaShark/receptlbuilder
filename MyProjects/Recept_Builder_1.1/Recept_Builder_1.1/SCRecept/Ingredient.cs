﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1
{
    public class Ingredient
    {
        String name;
        double amount = 0;
        String dimension;
        public Ingredient (String name)
        {
           this.Name = name;
            
        }

        public Ingredient(String name, double amount, String dimension) : this(name)
        {
            this.amount = amount;
            this.dimension = dimension;
        }

        public String Name
        {
            get {
                return this.name;
            }

            set
            {
                this.name = value;
            }
            
        }

        public double Amount
        {
            get {
                return this.amount;
            }

            set
            {
                this.amount = value;
            }
        }

        public String Dimenition
        {
            get {
                return this.dimension;
            }

            set
            {
                this.dimension = value;
            }
        }

        public override string ToString()
        {
            String objString = this.name;
            if (this.amount != 0 && this.dimension != null && this.dimension != "")
            {
                String amountS = this.amount.ToString();
                objString += " --- " + amountS + " " + this.dimension;
            }
            return objString;
        }
    }
}
