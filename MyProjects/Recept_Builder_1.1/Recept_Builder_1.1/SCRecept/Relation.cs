﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1
{
    public class Relation
    {
        public const String INGREDIENTS = "nrel_ingredients";
        public const String INSTRUCTION = "nrel_instruction";
        public const String NAME = "nrel_name";
        public const String IDENTIFER = "nrel_main_idtf";
        public const String AMOUNT = "nrel_amount";
        public const String VALUE = "nrel_value";
        public const String TRANSLATION = "nrel_translation";
        public const string DEVICES = "nrel_devices";
        public const string ACTIONS = "nrel_actions";
        public const string LEAD_TIME = "nrel_lead_time";
        public const string RESULT = "nrel_result";
        public const string INSERTION = "nrel_insertion";
        public const string DESCRIPTION = "nrel_insertion";
        public const string IMAGE = "nrel_insertion";
    }
}
