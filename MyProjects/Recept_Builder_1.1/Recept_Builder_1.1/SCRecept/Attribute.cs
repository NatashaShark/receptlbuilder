﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1
{
    public class Attribute
    {
        public const String RECEPT = "rrel_recept";
        public const String INGREDIENT = "rrel_ingredient";
        public const String INGREDIENT_SET = "rrel_ingredient_set";
        public const String INSTRUCTION_STEP = "rrel_instruction_step";
        public const String INSTRUCTION_SET = "rrel_instruction_set";
        public const String NAME = "rrel_name";

        public const String AMOUNT = "rrel_amount";
        public const String TRANSLATION = "rrel_translation";
        public const String VALUE = "rrel_value";
        public const string DEVICES = "rrel_devices";
        public const string ACTIONS = "rrel_actions";
        public const string LEAD_TIME = "rrel_lead_time";
        public const string RESULT = "rrel_result";
    }
}
