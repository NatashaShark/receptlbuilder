﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recept_Builder_1_1.SCRecept
{
    public class Result
    {
        private String name;
        private double amount = 0;
        private string dimension;

        public Result()
        {
        }

        public Result(String name) 
        {
            this.name = name;
        }
        public Result(String name, double amount, String dimension) : this(name)
        {
            this.amount = amount;
            this.dimension = dimension;
        }

        public String Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public double Amount
        {
            get
            {
                return this.amount;
            }

            set
            {
                this.amount = value;
            }
        }

        public String Dimenition
        {
            get
            {
                return this.dimension;
            }

            set
            {
                this.dimension = value;
            }
        }

        public override string ToString()
        {
            return this.name;
        }
    }
}
