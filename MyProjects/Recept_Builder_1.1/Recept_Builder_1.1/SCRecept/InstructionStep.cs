﻿using Recept_Builder_1_1.Ierarhie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recept_Builder_1_1.SCRecept
{
    public class InstructionStep
    {
        private List<Ingredient> ingredientList;
        private List<CoockingDevice> deviceList;
        private List<CoockingAction> actionList;
        private LeadTime leadTime;
        private Result result;
        private String translation;

        public InstructionStep()
        {
            this.ingredientList = new List<Ingredient>();
            this.deviceList = new List<CoockingDevice>();
            this.actionList = new List<CoockingAction>();
        }
        public InstructionStep (List<Ingredient> ingredientList, List<CoockingDevice> deviceList, List<CoockingAction> actionList, LeadTime leadTime, Result result, String translation) : this()
        {
            this.ingredientList = ingredientList;
            this.deviceList = deviceList;
            this.actionList = actionList;
            this.leadTime = leadTime;
            this.result = result;
            this.translation = translation;
        }

        public List<Ingredient> Ingredients 
        { 
            get
            {
                return this.ingredientList;
            }
            set
            {
                this.ingredientList = value;
            }
        }


        public List<CoockingDevice> Devices 
        { 
            get
            {
                return this.deviceList;
            }
            set
            {
                this.deviceList = value;
            }
        }
        public List<CoockingAction> Actions
        {
            get
            {
                return this.actionList;
            }
            set
            {
                this.actionList = value;
            }
        }

        public LeadTime LeadTime
        {
            get
            {
                return this.leadTime;
            }
            set
            {
                this.leadTime = value;
            }
        }

        public Result Result
        {
            get
            {
                return this.result;
            }
            set
            {
                this.result = value;
            }
        }

        public String Translation
        {
            get
            {
                return this.translation;
            }
            set
            {
                this.translation = value;
            }
        }

        public override string ToString()
        {
            return this.translation;
        }
    }
}
