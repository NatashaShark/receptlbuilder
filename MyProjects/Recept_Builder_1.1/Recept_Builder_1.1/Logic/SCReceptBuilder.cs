﻿using Recept_Builder_1_1.Ierarhie;
using Recept_Builder_1_1.SCRecept;
using sctp_client;
using sctp_client.Arguments;
using sctp_client.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1.Logic
{
    public class SCReceptBuilder : SCBuilder
    {
        public SCReceptBuilder(CommandPool pool)
            : base(pool)
        {

        }
        public void CreateNodeFromModel(Recept_Builder_1._1.Recept recept)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            ScAddress sysReceptNodeAddress = GetSysNodeAddress("recept");

            ScAddress receptNodeAddress = GenNodeAddress();
            Command.SetSysId(receptNodeAddress, TranslitBuilder.GetTranslit(recept.Name));
            ACommand sysToCommonArc = Command.CreateArc(ElementType.sc_type_arc_pos_const_perm_acc, sysReceptNodeAddress, receptNodeAddress);

            //составление рецепта
            GenIdentificatorConstruction(receptNodeAddress, recept.Name);

            ScAddress ingredientSetNodeAddress = GenNodeAddress();
            GenBinaryRelation(receptNodeAddress, ingredientSetNodeAddress, Relation.INGREDIENTS, Attribute.RECEPT, Attribute.INGREDIENT_SET);
            List<Node> ingredientNodes = GenIngredientsBranch(recept.Ingredients, ingredientSetNodeAddress);

            ScAddress instructionNodeAddress = GenNodeAddress();
            GenBinaryRelation(receptNodeAddress, instructionNodeAddress, Relation.INSTRUCTION, Attribute.RECEPT, Attribute.INSTRUCTION_SET);

            ScAddress receptResultAddress = GenNodeWithRelation(receptNodeAddress, Relation.RESULT, Attribute.RECEPT, Attribute.RESULT);
            GenIdentificatorConstruction(receptResultAddress, recept.Result.Name);
            GenInstructionBranch(recept.Instruction, ingredientNodes, instructionNodeAddress, receptResultAddress);

            watch.Stop();
        }

        

        private void GenDevicesBranch(List<CoockingDevice> deviceList, ScAddress devicesNodeAddress)
        {
            foreach (CoockingDevice device in deviceList)
            {
                ScAddress deviceNodeSCAddress = GenNodeAddress();
                GenAccessorArc(devicesNodeAddress, deviceNodeSCAddress, null);
                GenIdentificatorConstruction(deviceNodeSCAddress, device.Name);
            }
        }

        private void GenActionsBranch(List<CoockingAction> actionList, ScAddress actionsNodeAddress)
        {
            foreach (CoockingAction action in actionList)
            {
                ScAddress actionNodeSCAddress = GenNodeAddress();
                GenAccessorArc(actionsNodeAddress, actionNodeSCAddress, null);
                GenIdentificatorConstruction(actionNodeSCAddress, action.Name);
            }
        }



        private List<Node> GenIngredientsBranch(List<Ingredient> ingredientList, ScAddress ingredientsNodeAddress)
        {
            List<Node> nodes = new List<Node>();
            foreach (Ingredient ingredient in ingredientList)
            {
                ScAddress ingredientNodeAddress = GenElementOfsystemSet(ingredient.Name);
                GenAccessorArc(ingredientsNodeAddress, ingredientNodeAddress, null);
                GenValueConstruction(ingredient.Amount, ingredient.Dimenition, ingredientNodeAddress);

                nodes.Add(new Node(ingredient.Name, ingredientNodeAddress));
            }
            return nodes;
        }

        private void GenInstructionBranch(List<InstructionStep> instruction, List<Node> ingredientNodes, ScAddress instructionNodeAddress, ScAddress receptResultAddress)
        {
            if (instruction != null)
            {
                List<Node> resultNodes = new List<Node>();
                for (int stepIndex = 0; stepIndex < instruction.Count; stepIndex++)
                {
                    InstructionStep instructionStep = instruction[stepIndex];
                    ScAddress instructionStepNodeAddress = GenNodeAddress();
                    GenAccessorArc(instructionNodeAddress, instructionStepNodeAddress, (stepIndex + 1).ToString());

                    ScAddress ingredientsNodeAddress = GenNodeWithRelation(instructionNodeAddress, Relation.INGREDIENTS, Attribute.INSTRUCTION_STEP, Attribute.INGREDIENT_SET);
                    foreach (Ingredient stepIngredient in instructionStep.Ingredients)
                    {
                        ScAddress? ingridientNode = GetNodeByName(stepIngredient.Name, ingredientNodes);
                        if (ingridientNode == null)
                        {
                            ingridientNode = GetNodeByName(stepIngredient.Name, resultNodes);
                        }
                        if (ingridientNode != null)
                        {
                            ScAddress ingredientNodeAddress = (ScAddress)ingridientNode;
                            GenAccessorArc(ingredientsNodeAddress, ingredientNodeAddress, null);
                        }

                    }

                    ScAddress devicesNodeAddress = GenNodeWithRelation(instructionNodeAddress, Relation.DEVICES, Attribute.INSTRUCTION_SET, Attribute.DEVICES);
                    GenDevicesBranch(instructionStep.Devices, devicesNodeAddress);

                    ScAddress actionsNodeAddress = GenNodeWithRelation(instructionNodeAddress, Relation.ACTIONS, Attribute.INSTRUCTION_SET, Attribute.ACTIONS);
                    GenActionsBranch(instructionStep.Actions, actionsNodeAddress);

                    ScAddress leadTimeNodeAddress = GenNodeWithRelation(instructionNodeAddress, Relation.LEAD_TIME, Attribute.INSTRUCTION_SET, Attribute.LEAD_TIME);
                    GenValueConstruction(instructionStep.LeadTime.Amount, instructionStep.LeadTime.Dimention, leadTimeNodeAddress);

                    if (stepIndex < instruction.Count - 1)
                    {
                        ScAddress resultNodeAddress = GenNodeWithRelation(instructionNodeAddress, Relation.RESULT, Attribute.INSTRUCTION_SET, Attribute.RESULT);
                        GenIdentificatorConstruction(resultNodeAddress, instructionStep.Result.Name);
                        resultNodes.Add(new Node(instructionStep.Result.Name, resultNodeAddress));
                    }
                    else
                    {
                        GenBinaryRelation(instructionNodeAddress, receptResultAddress, Relation.RESULT, Attribute.INSTRUCTION_SET, Attribute.RESULT);
                    }

                    ScAddress traslationNodeAddress = GenNodeWithRelation(instructionStepNodeAddress, Relation.TRANSLATION, null, Attribute.TRANSLATION);
                    GenNodeWithContent(traslationNodeAddress, instructionStep.Translation);
                }
            }
            else
            {
                throw new ArgumentNullException("Instruction list are empty.");
            }
        }
    }
}
