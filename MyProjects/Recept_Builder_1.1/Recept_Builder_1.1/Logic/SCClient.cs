﻿using System;
using sctp_client;
using sctp_client.Commands;
using sctp_client.Responses;
using sctp_client.Arguments;
using System.Diagnostics;
using System.Collections.Generic;
using Recept_Builder_1_1.SCRecept;
using Recept_Builder_1_1.Ierarhie;
using Recept_Builder_1_1.Logic;

namespace Recept_Builder_1_1
{
    public class SCClient : Recept_Builder_1_1.Logic.SCReceptSender
    {
        private const String SERVER_IP = "127.0.0.1";
        private const int PORT = 55770;

        private static SCClient instance;

        private CommandPool pool;
        private SCReceptBuilder receptBuilder;
        private CookingElementBuilder elementBuilder;

        
        private SCClient () {
            pool = new CommandPool(SERVER_IP, PORT, ClientType.SyncClient);
            receptBuilder = new SCReceptBuilder(pool);
            elementBuilder = new CookingElementBuilder(pool);
        }

        public static SCClient Instance
        {
            get
            {
                if (instance == null)
                {
                    if (instance == null)
                    {
                        instance = new SCClient();
                    }
                }
                return instance;
            }
        }


        public void SendRecept(Recept_Builder_1._1.Recept recept)
        {
            receptBuilder.CreateNodeFromModel(recept);            
        }

        public void SendCookingelement(CookingElement model)
        {
            elementBuilder.CreateNodeFromModel(model);
        }  
        
    }

    
}
