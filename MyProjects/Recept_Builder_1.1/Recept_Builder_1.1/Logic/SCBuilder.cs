﻿using sctp_client;
using sctp_client.Arguments;
using sctp_client.Commands;
using sctp_client.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1.Logic
{
    public abstract class SCBuilder
    {
        protected SCBuilder(CommandPool pool)
        {
            this.Pool = pool;
        }
        protected CommandPool Pool { get; set; }
        protected struct Node
        {
            public string name;
            public ScAddress node;

            public Node(string name, ScAddress node)
            {
                this.name = name;
                this.node = node;
            }
        }
        protected ScAddress? GetNodeByName(string name, List<Node> nodes)
        {
            if (nodes == null || nodes.Count == 0)
            {
                throw new ArgumentNullException("Nodes list is empty.");
            }
            ScAddress? node = nodes[0].node;

            int inrexIfNode = 0;
            bool find = true;
            while (inrexIfNode < nodes.Count && !find)
            {
                if (name == nodes[inrexIfNode].name)
                {
                    node = nodes[inrexIfNode].node;
                    find = true;
                }
                inrexIfNode++;
            }
            if (!find)
            {
                return null;
                throw new Exception("Nodes set dosen't contain search item.");
            }
            return node;
        }

        protected ScAddress GenNodeWithRelation(ScAddress firstNodeAddress, string relation, string attr1, string attr2)
        {
            ScAddress secondNodeAddress = GenNodeAddress();
            GenBinaryRelation(firstNodeAddress, secondNodeAddress, relation, attr1, attr2);
            return secondNodeAddress;
        }



        protected ScAddress GenElementOfsystemSet(string setName)
        {
            ScAddress sysSetAddress = GetSysNodeAddress(setName);
            ScAddress elementNodeAddress = GenNodeAddress();
            GenAccessorArc(sysSetAddress, elementNodeAddress, null);
            return elementNodeAddress;
        }

        protected void GenBinaryRelation(ScAddress firstNodeAddress, ScAddress secondNodeAddress, String relationName, String firstAttributeName, String secondAttributeName)
        {
            if (relationName == null)
            {
                return;
            }
            ScAddress nrelNodeAddress = GenNodeAddress();
            ScAddress ligamentNodeAddress = GenNodeAddress();
            ACommand nrelArc = Command.CreateArc(ElementType.sc_type_arc_pos, nrelNodeAddress, ligamentNodeAddress);
            ScAddress nrelArcAddress = (nrelArc.Response as RspCreateArc).CreatedArcAddress;
            Command.SetSysId(nrelNodeAddress, relationName);
            ACommand nrelToLigamentArc = Command.CreateArc(ElementType.sc_type_arc_pos_const_perm_acc, nrelArcAddress, ligamentNodeAddress);
            GenIdentificatorConstruction(nrelNodeAddress, relationName);
            Pool.Send(nrelArc);
            Pool.Send(nrelToLigamentArc);

            GenAccessorArc(ligamentNodeAddress, firstNodeAddress, firstAttributeName);
            GenAccessorArc(ligamentNodeAddress, secondNodeAddress, secondAttributeName);

        }

        protected void GenAccessorArc(ScAddress setNodeAddress, ScAddress elementNodeAddress, string attributeName)
        {
            ACommand setToElementNodeArc = Command.CreateArc(ElementType.sc_type_arc_access, setNodeAddress, elementNodeAddress);
            Pool.Send(setToElementNodeArc);
            ScAddress setToElementNodeArcAddress = (setToElementNodeArc.Response as RspCreateArc).CreatedArcAddress;
            if (attributeName != null)
            {
                ScAddress attributeNodeAddress = GenNodeAddress();
                ACommand attributeArc = Command.CreateArc(ElementType.sc_type_arc_access, attributeNodeAddress, setToElementNodeArcAddress);
                GenIdentificatorConstruction(attributeNodeAddress, attributeName);
                Pool.Send(attributeArc);
            }
        }

        protected void GenValueConstruction(double amount, string dimenition, ScAddress nodeAddress)
        {
            if (amount > 0 && dimenition != null && dimenition != "")
            {
                ScAddress amountNodeAddress = GenNodeAddress();
                GenBinaryRelation(nodeAddress, amountNodeAddress, Relation.AMOUNT, Attribute.INGREDIENT, Attribute.AMOUNT);
                //построение отношения численного значения
                ScAddress valueNodeAddress = GenNodeAddress();
                GenBinaryRelation(valueNodeAddress, amountNodeAddress, Relation.VALUE, Attribute.VALUE, Attribute.AMOUNT);

                ScAddress specificValueNodeAddress = GenNodeAddress();
                Command.SetLinkContent(specificValueNodeAddress, (amount).ToString());
                GenAccessorArc(valueNodeAddress, specificValueNodeAddress, dimenition);
            }
        }




        protected ScAddress GenNodeAddress()
        {
            ACommand node = Command.CreateNode(ElementType.sc_type_node_const);
            Pool.Send(node);
            ScAddress nodeAddress = (node.Response as RspCreateNode).CreatedNodeAddress;
            return nodeAddress;
        }


        protected ScAddress GetSysNodeAddress(String name)
        {
            String sysID = TranslitBuilder.GetTranslit(name);
            ACommand sysNode = Command.FindElementById(sysID);
            ScAddress sysNodeAddress = new ScAddress();
            if (sysNode == null)
            {
                sysNodeAddress = GenNodeAddress();
                Command.SetSysId(sysNodeAddress, sysID);
                GenIdentificatorConstruction(sysNodeAddress, name);
            }
            else
            {
                sysNodeAddress = (sysNode.Response as RspCreateNode).CreatedNodeAddress;
            }
            return sysNodeAddress;
        }


        protected void GenNodeWithContent(ScAddress translationSetNodeAddress, string traslationText)
        {
            ScAddress specificTranslationNodeAddress = GenNodeAddress();
            GenAccessorArc(translationSetNodeAddress, specificTranslationNodeAddress, null);
            Command.SetLinkContent(specificTranslationNodeAddress, traslationText);
        }

        protected void GenIdentificatorConstruction(ScAddress nodeAddress, String name)
        {
            ScAddress nameNodeAddress = GenNodeAddress();
            GenBinaryRelation(nodeAddress, nameNodeAddress, Relation.NAME, null, Attribute.NAME);
            Command.SetLinkContent(nameNodeAddress, name);
        }


        protected void GenNodeWithIerarhie(Ierarhie.CookingElement model, ScAddress node)
        {
            if (model.Superset != null)
            {
                ScAddress superset = GetSysNodeAddress(model.Superset.Name);
                GenBinaryRelation(superset, node, Relation.INSERTION, null, null);
                GenElementOfsystemSet(model.Superset.Name);
                GenNodeWithIerarhie(model.Superset, superset);
            }
        }
    }
}
