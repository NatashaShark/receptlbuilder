﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1.Logic
{
    public interface SCReceptSender
    {
       void SendRecept(Recept_Builder_1._1.Recept recept);
    }
}
