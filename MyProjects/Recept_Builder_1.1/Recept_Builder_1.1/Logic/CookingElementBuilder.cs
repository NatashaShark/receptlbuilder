﻿using sctp_client;
using sctp_client.Arguments;
using sctp_client.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1.Logic
{
    public class CookingElementBuilder: SCBuilder
    {
        public CookingElementBuilder(CommandPool pool) : base(pool)
        {
        }

        public void CreateNodeFromModel(Ierarhie.CookingElement model)
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();
            

            ScAddress modelNodeAddress = GenNodeAddress();
            GenIdentificatorConstruction(modelNodeAddress, model.Name);
            GenNodeWithIerarhie(model, modelNodeAddress);

            foreach (string description in model.Descriptions) 
            {
                ScAddress descriptionNodeAddress = GenNodeWithRelation(modelNodeAddress, Relation.DESCRIPTION, null, null);
                GenNodeWithContent(descriptionNodeAddress, description);
            }

            foreach (string url in model.ImageURLs)
            {
                ScAddress urlNodeAddress = GenNodeWithRelation(modelNodeAddress, Relation.IMAGE, null, null);
                GenNodeWithContent(urlNodeAddress, url);
            }


            watch.Stop();
        }

       
    }
}
