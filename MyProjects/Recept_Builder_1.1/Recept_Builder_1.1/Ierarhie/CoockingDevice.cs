﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Recept_Builder_1_1.Ierarhie
{
    public class CoockingDevice : CookingElement
    {

        public CoockingDevice(String name) 
        {
            this.Name = name;
        }

        public String Name
        {
            get;
            set;
        }

        public CookingElement Superset
        {
            get;
            set;
        }

        public IEnumerable<string> ImageURLs
        {
            get;
            set;
        }

        public IEnumerable<string> Descriptions
        {
            get;
            set;
        }
    }
}
