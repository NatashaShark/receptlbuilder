﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Recept_Builder_1_1.Ierarhie
{
    public interface CookingElement
    {
        CookingElement Superset { get; set; }
        string Name { get; set; }
        IEnumerable<string> ImageURLs { get; set; }
        IEnumerable<string> Descriptions { get; set; }
    }
}
