﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recept_Builder_1_1.GUI
{
    class ItemOfTable : Panel
    {
        TextBox ingredientColumn;
        TextBox amountColumn;
        TextBox dimenitionColumn;

        public const int ColHeight = 25;
        public const int Margin= 10;
        public ItemOfTable()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.AutoSize = true;
            this.Height = ColHeight;
            ingredientColumn = new TextBox();
            ingredientColumn.Height = ColHeight;
            amountColumn = new TextBox();
            amountColumn.Height = ColHeight;
            dimenitionColumn = new TextBox();
            dimenitionColumn.Height = ColHeight;
            this.Controls.Add(ingredientColumn);
            this.Controls.Add(amountColumn);
            this.Controls.Add(dimenitionColumn);
        }

        public TextBox IngredientTextBox
        {
            get
            {
                return ingredientColumn;
            }
        }

        public Ingredient Ingredient (){
            if (ingredientColumn.Text.Trim() != "")
            {
                String name = ingredientColumn.Text.Trim();
                double amount = 0;
                try
                {
                    amount = Convert.ToDouble(amountColumn.Text.Trim());
                }
                catch
                {

                }
                String dimenition = dimenitionColumn.Text.Trim();
                if (dimenition == "")
                {
                    dimenition = null;
                }
                Ingredient ingredient = new Ingredient(name, amount, dimenition);
                return ingredient;
                
            }
            return null;
        }

        public void ColumnsWidth(int width1, int width2, int width3)
        {
            ingredientColumn.Width = width1;
            ingredientColumn.Left = 0;
            amountColumn.Width = width2;
            amountColumn.Left = width1 + Margin;
            dimenitionColumn.Width = width3;
            dimenitionColumn.Left = width1 + Margin + width2 + Margin;
        }

        public void ColumnsText(String text1, String text2, String text3)
        {
            ingredientColumn.Text = text1;
            amountColumn.Text = text2;
            dimenitionColumn.Text = text3;
        }
    }
}
