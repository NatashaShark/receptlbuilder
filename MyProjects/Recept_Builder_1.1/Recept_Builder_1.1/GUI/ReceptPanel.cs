﻿using Recept_Builder_1._1;
using Recept_Builder_1_1.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recept_Builder_1_1.GUI
{
    class ReceptPanel : Panel
    {
        TextBox nameTextArea;
        Table ingredientsTable;
        RichTextBox instructionsTextArea;
        Button addReceptButton;        

        public ReceptPanel()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.AutoScroll = true;
            this.MinimumSize = new Size(400, 490);
            this.BackColor = Color.Beige;

            Panel header = new Panel();
            header.Width = this.Width;
            header.Height = 54;
            header.BackColor = Color.FromArgb(173, 255, 47);
            this.Controls.Add(header);

            nameTextArea = new TextBox();
            nameTextArea.Left = 10;
            nameTextArea.Top = 15;
//            nameTextArea.Width = this.Width - 20;
            nameTextArea.Height = 14;
            nameTextArea.Font = LoadFont(12);
            nameTextArea.Text = "Название";
            nameTextArea.MaxLength = 45;
            header.Controls.Add(nameTextArea);

            Label ingredientsLabel = createLabel("Список ингредиентов:", 60);
            this.Controls.Add(ingredientsLabel);
            int Col1Width = 180;
            int Col2Width = 70;
            int Col3Width = 70;
            Panel tableHeader = createHeader(Col1Width, Col2Width, Col3Width);
            tableHeader.Top = 80;
          //  tableHeader.Width = Col1Width + Col2Width + Col3Width + ItemOfTable.Margin * 2;
            tableHeader.Height = 25;
            tableHeader.Left = 10;
            this.Controls.Add(tableHeader);
            ingredientsTable = new Table();
            ingredientsTable.BackColor = Color.Beige;
            ingredientsTable.ColumnsWidth(Col1Width, Col2Width, Col3Width);
            ingredientsTable.Top = 105;
            ingredientsTable.Left = 10;
            ingredientsTable.Width = this.Width - 20;
            ingredientsTable.Height = 5 * ItemOfTable.ColHeight;
            this.Controls.Add(ingredientsTable);

            Label instructionLabel = createLabel("Способ приготовления:", 230);            
            this.Controls.Add(instructionLabel);
            instructionsTextArea = new RichTextBox();
            instructionsTextArea.Font = LoadFont(8);
            instructionsTextArea.Location = new Point(10, 250);
            instructionsTextArea.Height = 180;
            instructionsTextArea.Width = this.Width - 20;
            this.Controls.Add(instructionsTextArea);

            addReceptButton = new Button();
            addReceptButton.Font = LoadFont(10);
            addReceptButton.Text = "Добавить";
            addReceptButton.Width = 100;
            addReceptButton.Height = 25;
            addReceptButton.Location = new Point(this.Width / 2 - 50, 450);
            addReceptButton.Click += new EventHandler(button_Click);
            this.Controls.Add(addReceptButton);
        }

        private Label createLabel(string text, int top)
        {
            Label label = new Label();
            label.Text = text;
            label.Font = LoadFont(10);
            label.AutoSize = true;
            label.Top = top;
            label.Left = 10;
            return label;
        }

        private Panel createHeader(int width1, int width2, int width3)
        {
            Panel header = new Panel();
            header.Height = ItemOfTable.ColHeight;
            Label ingredientLabel = new Label();
            ingredientLabel.Text = "Ингредиент";
            ingredientLabel.Font = LoadFont(8);
            ingredientLabel.Width = width1;
            Label amountLabel = new Label();
            amountLabel.Left = width1 + ItemOfTable.Margin;
            amountLabel.Text = "Количество";
            amountLabel.Font = LoadFont(8);
            amountLabel.Width = width2;
            Label dimenitionLabel = new Label();
            dimenitionLabel.Left = width1 + width2 + ItemOfTable.Margin * 2;
            dimenitionLabel.Text = "Мера";
            dimenitionLabel.Font = LoadFont(8);
            dimenitionLabel.Width = width3;
            header.Controls.Add(ingredientLabel);
            header.Controls.Add(amountLabel);
            header.Controls.Add(dimenitionLabel);
            return header;
        }

        private void button_Click(object sender, EventArgs e)
        {
            Recept_Builder_1_1.Logic.SCReceptSender client = SCClient.Instance;
            Recept recept = new Recept(this.ReceptName, this.Ingredients, null, new Recept_Builder_1_1.SCRecept.Result(this.ReceptName));
            client.SendRecept(recept);
        }

        private string ReceptName
        {
            get 
            {
                String name = nameTextArea.Text;
                return name;
            }
            
        }

        private List<Ingredient> Ingredients
        {
            get
            {
                List<Ingredient> ingredients = new List<Ingredient>();
                foreach (ItemOfTable item in ingredientsTable.Items)
                {
                    Ingredient ingredient = item.Ingredient();
                    if (ingredient != null)
                    {
                        ingredients.Add(ingredient);
                    }                    
                }
                return ingredients;
            }
        }

        private string Instruction
        {
            get {
                String instructions = instructionsTextArea.Text;
                return instructions;
            }
            
        }

        public Button DoButtonLisner
        {
            get 
            { 
                return addReceptButton; 
            }
        }

        Font LoadFont(int size)
        {
            PrivateFontCollection _fonts = new PrivateFontCollection();
            byte[] fontData = Resources.HelveticaLight;
            IntPtr fontPtr = Marshal.AllocCoTaskMem(fontData.Length);
            Marshal.Copy(fontData, 0, fontPtr, fontData.Length);
            _fonts.AddMemoryFont(fontPtr, fontData.Length);
            Marshal.FreeCoTaskMem(fontPtr);
            Font customFont = new Font(_fonts.Families[0], size);
            return customFont;
        }
    }
}
