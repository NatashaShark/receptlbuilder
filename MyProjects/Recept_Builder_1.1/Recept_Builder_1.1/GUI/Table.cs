﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Recept_Builder_1_1.GUI
{
    class Table : Panel
    {
        private List<ItemOfTable> items;
        private int Col1Width;
        private int Col2Width;
        private  int Col3Width;
        private Panel columns;

        private EventHandler reserveEventHandler;        

        public Table() 
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.Col1Width = 200;
            this.Col2Width = 100;
            this.Col3Width = 100;
            this.AutoScroll = true;
            this.items = new List<ItemOfTable>();

            columns = new Panel();
            columns.Height = 0;          
            this.Controls.Add(columns);

            addItem();
            addItem();
            ItemOfTable reserveItem = addItem();
            reserveEventHandler = new EventHandler(onReserveItemClick);
            reserveItem.IngredientTextBox.Click += reserveEventHandler;
            reserveItem.IngredientTextBox.TextChanged += reserveEventHandler;

        }
        public ItemOfTable addItem()
        {
            ItemOfTable item = new ItemOfTable();
            item.Top = ItemOfTable.ColHeight * items.Count;
            item.ColumnsWidth(Col1Width, Col2Width, Col3Width);
            items.Add(item);
            columns.Controls.Add(item);
            columns.Height += ItemOfTable.ColHeight;
            return item;
        }

        public void ColumnsWidth(int width1, int width2, int width3)
        {
            this.Col1Width = width1;
            this.Col2Width = width2;
            this.Col3Width = width3;
            foreach (ItemOfTable item in items)
            {
                item.ColumnsWidth(width1, width2, width3);
            }
            columns.Width = Col1Width + Col2Width + Col3Width + ItemOfTable.Margin * 2;
            
        }

        private void onReserveItemClick(object sender, EventArgs e)
        {
            TextBox lastItem = (TextBox)sender;
            lastItem.Click -= reserveEventHandler;
            lastItem.TextChanged -= reserveEventHandler;
            ItemOfTable newReserveItem = addItem();
            newReserveItem.IngredientTextBox.Click += reserveEventHandler;
            newReserveItem.TextChanged -= reserveEventHandler;
        }

        public List<ItemOfTable> Items
        {
            get
            {
                return items;
            }
        }

    }
}
