﻿
namespace sctp_client.Arguments
{

   internal enum ConstrTemplateType:byte
    {
    t_3F_A_A = 0,
    t_3A_A_F = 1,
    t_3F_A_F = 2,
    t_5F_A_A_A_F = 3,
    t_5A_A_F_A_F = 4,
    t_5F_A_F_A_F = 5,
    t_5F_A_F_A_A = 6,
    t_5F_A_A_A_A = 7,
    t_5A_A_F_A_A = 8,
    }
}
